{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GADTs #-}

module PS
  ( PSEff(..)
  , PS
  , newRef
  , readRef
  , writeRef
  , newArray
  , readArray
  , writeArray
  , getArrayLength
  , newArrayFromList
  , arrayToList
  , trace
  , RefEq(..)
  , ArrEq(..)
  ) where

import Data.Foldable (for_)
import FreeP (FreeP, liftFreeP)
import Runners (Parallelizable(..))

data PSEff ref arr a where
  NewRef :: v -> PSEff ref arr (ref v)
  ReadRef :: ref v -> PSEff ref arr v
  WriteRef :: ref v -> v -> PSEff ref arr ()

  NewArray :: Int -> v -> PSEff ref arr (arr v)
  ReadArray :: arr v -> Int -> PSEff ref arr v
  WriteArray :: arr v -> Int -> v -> PSEff ref arr ()
  GetArrayLength :: arr v -> PSEff ref arr Int

  Trace :: String -> PSEff ref arr ()

type PS ref arr = FreeP (PSEff ref arr)

newRef :: v -> PS ref arr (ref v)
newRef v = liftFreeP (NewRef v)

readRef :: ref v -> PS ref arr v
readRef var = liftFreeP (ReadRef var)

writeRef :: ref v -> v -> PS ref arr ()
writeRef ref v = liftFreeP (WriteRef ref v)

newArray :: Int -> v -> PS ref arr (arr v)
newArray n initial = liftFreeP (NewArray n initial)

readArray :: arr a -> Int -> PS ref arr a
readArray arr i = liftFreeP (ReadArray arr i)

writeArray :: arr v -> Int -> v -> PS ref arr ()
writeArray arr i v = liftFreeP (WriteArray arr i v)

getArrayLength :: arr v -> PS ref arr Int
getArrayLength arr = liftFreeP (GetArrayLength arr)

trace :: String -> PS ref arr ()
trace v = liftFreeP (Trace v)

arrBottomEl :: a
arrBottomEl = error "PS: undefined array element"

newArrayFromList :: [v] -> FreeP (PSEff ref arr) (arr v)
newArrayFromList list = do
  let n = length list
  arr <- newArray n arrBottomEl
  for_ (zip [0..] list) $ \(i, el) -> writeArray arr i el
  pure arr

arrayToList :: arr a -> PS ref arr [a]
arrayToList arr = do
  n <- getArrayLength arr
  traverse (readArray arr) [0..n - 1]

class RefEq ref where
  refEq :: ref a -> ref b -> Bool

class ArrEq arr where
  arrEq :: arr a -> arr b -> Bool

instance (RefEq ref, ArrEq arr) => Parallelizable (PSEff ref arr) where
  independent (ReadRef ref) (WriteRef ref' _) = not (refEq ref ref')
  independent (WriteRef ref _) (ReadRef ref') = not (refEq ref ref')
  independent (WriteRef ref _) (WriteRef ref' _) = not (refEq ref ref')
  independent (ReadArray arr i) (WriteArray arr' i' _)
    = not (arrEq arr arr' && i == i')
  independent w@WriteArray{} r@ReadArray{} = independent r w
  independent (WriteArray arr i _) (WriteArray arr' i' _)
    = not (arrEq arr arr' && i == i')
  independent _ _ = True

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}

module PS.IO
  ( PSIO
  , PSIOEff
  , PSRef
  , PSArr
  , runPSIO
  , Exec
  , exec
  , delayedExec
  , buildGraph
  ) where

import Control.Concurrent (threadDelay)
import Control.Monad (foldM_, join)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.IO.Unlift (MonadUnliftIO)
import Control.Monad.ST (RealWorld)
import Data.Array.IArray (IArray, bounds)
import qualified Data.Array.IArray as Array ((!))
import Data.HashMap.Lazy (HashMap)
import qualified Data.HashMap.Lazy as Map (empty, insert, lookup)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Data.Primitive.Array (MutableArray, newArray, readArray, sizeofMutableArray, writeArray)
import Data.Unique (Unique, newUnique)
import GraphT (Eff, GraphT, connect, inspectEff, inspectNodes)

import PS (ArrEq(..), PSEff(..), RefEq(..))

type ID = Unique

data PSRef a = PSRef
  { psIORef :: IORef a
  , psrID :: ID
  }

instance RefEq PSRef where
  refEq (PSRef _ i) (PSRef _ j) = i == j

data PSArr a = PSArr
  { psArr :: MutableArray RealWorld a
  , psaID :: ID
  }

instance ArrEq PSArr where
  arrEq (PSArr _ i) (PSArr _ j) = i == j

newtype PSIO a = PSIO (IO a)
  deriving newtype (Functor, Applicative, Monad, MonadIO, MonadUnliftIO)

type PSIOEff = PSEff PSRef PSArr

type Exec = forall a. PSIOEff a -> PSIO a

exec :: Exec
exec (NewRef v) = liftIO (PSRef <$> newIORef v <*> newUnique)
exec (ReadRef ref) = liftIO (readIORef (psIORef ref))
exec (WriteRef ref v) = liftIO (writeIORef (psIORef ref) v)
exec (NewArray n initial) = liftIO (PSArr <$> newArray n initial <*> newUnique)
exec (ReadArray arr i) = liftIO (readArray (psArr arr) i)
exec (WriteArray arr i v) = liftIO (writeArray (psArr arr) i v)
exec (GetArrayLength arr) = pure (sizeofMutableArray (psArr arr))
exec (Trace v) = liftIO (putStrLn v)

delayedExec :: Int -> Exec
delayedExec microsecs eff = liftIO (threadDelay microsecs) *> exec eff

-- don't want s to escape, it could lead to RefCnt reset
runPSIO :: PSIO a -> IO a
runPSIO (PSIO psio) = psio

type LastRead = HashMap ID Int
type LastWrite = HashMap ID Int
type LastReadArr = HashMap (ID, Int) Int
type LastWriteArr = HashMap (ID, Int) Int
type LastOps = (LastRead, LastWrite, LastReadArr, LastWriteArr)
type PSIOGraph = GraphT PSIOEff PSIO

buildGraph :: PSIOGraph ()
buildGraph = join (inspectNodes inspector)
  where
    inspector
      :: IArray arr (Eff PSIOEff) => arr Int (Eff PSIOEff) -> PSIOGraph ()
    inspector nodes = foldM_ processNode initLastOps [st..fin]
      where
        initLastOps = (Map.empty, Map.empty, Map.empty, Map.empty)
        (st, fin) = bounds nodes

        processNode :: LastOps -> Int -> PSIOGraph LastOps
        processNode lastOps@(lastR, lastW, lastRA, lastWA) i
          = inspectEff handleEff (nodes Array.! i)
          where
            handleEff :: PSIOEff b -> PSIOGraph LastOps
            handleEff NewRef{} = pure lastOps
            handleEff (ReadRef ref) = handleRead (psrID ref)
            handleEff (WriteRef ref _) = handleWrite (psrID ref)
            handleEff NewArray{} = pure lastOps
            handleEff (ReadArray arr ind) = handleReadArr (psaID arr) ind
            handleEff (WriteArray arr ind _) = handleWriteArr (psaID arr) ind
            handleEff GetArrayLength{} = pure lastOps
            handleEff Trace{} = pure lastOps

            handleRead :: ID -> PSIOGraph LastOps
            handleRead refI = do
              case Map.lookup refI lastR of
                Just prevReadI -> connect prevReadI i
                Nothing -> pure ()
              let lastR' = Map.insert refI i lastR
              pure (lastR', lastW, lastRA, lastWA)

            handleWrite :: ID -> PSIOGraph LastOps
            handleWrite refI = do
              case Map.lookup refI lastR of
                Just prevReadI -> connect prevReadI i
                Nothing -> pure ()
              case Map.lookup refI lastW of
                Just prevWriteI -> connect prevWriteI i
                Nothing -> pure ()
              let lastW' = Map.insert refI i lastW
              pure (lastR, lastW', lastRA, lastWA)

            handleReadArr :: ID -> Int -> PSIOGraph LastOps
            handleReadArr arrID ind = do
              case Map.lookup (arrID, ind) lastRA of
                Just prevReadI -> connect prevReadI i
                Nothing -> pure ()
              let lastRA' = Map.insert (arrID, ind) i lastRA
              pure (lastR, lastW, lastRA', lastWA)

            handleWriteArr :: ID -> Int -> PSIOGraph LastOps
            handleWriteArr arrID ind = do
              case Map.lookup (arrID, ind) lastRA of
                Just prevReadI -> connect prevReadI i
                Nothing -> pure ()
              case Map.lookup (arrID, ind) lastWA of
                Just prevWriteI -> connect prevWriteI i
                Nothing -> pure ()
              let lastWA' = Map.insert (arrID, ind) i lastWA
              pure (lastR, lastW, lastRA, lastWA')

{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module PS.String
  ( represent
  ) where

import Control.Monad.Primitive (PrimMonad(..), liftPrim)
import Control.Monad.Reader (MonadReader, ReaderT, ask, runReaderT)
import Control.Monad.ST (ST, runST)
import Control.Monad.Writer (MonadWriter, WriterT, execWriterT, tell)
import Data.Function ((&))
import Data.STRef (STRef, modifySTRef', newSTRef, readSTRef)
import Data.Word (Word64)
import FreeP (Free(..), FreeP, runFreeAp)
import Text.Printf (printf)

import PS (PSEff(..))

type ID = Word64
type IDCnt = ID

newtype PSString s a = PSString (ReaderT (STRef s IDCnt) (WriterT String (ST s)) a)
  deriving newtype ( Functor
                   , Applicative
                   , Monad
                   , MonadReader (STRef s IDCnt)
                   , MonadWriter String
                   )

instance PrimMonad (PSString s) where
  type PrimState (PSString s) = s
  primitive = PSString . primitive

data ConstRef a = ConstRef
  { crElem :: a
  , crID :: ID
  }

data ConstArr a = ConstArr
  { caSize :: Int
  , caElem :: a
  , caID :: ID
  }

type PSStringEff = PSEff ConstRef ConstArr

represent :: FreeP PSStringEff a -> String
represent freep = runST $ let (PSString m) = run freep in do
  cnt <- newSTRef 0
  m & flip runReaderT cnt & execWriterT

run :: FreeP PSStringEff a -> PSString s a
run (Last fa) = tell "Last ( " *> runFreeAp exec fa <* tell ")"
run (Step fx next) = do
  tell "Step ( "
  x <- runFreeAp exec fx
  tell ")\n"
  run (next x)

exec :: PSStringEff a -> PSString s a
exec eff = tell "(" *> cleanExec eff <* tell ") "

cleanExec :: PSStringEff a -> PSString s a
cleanExec (NewRef v) = do
  idRef <- ask
  id_ <- liftPrim (readSTRef idRef)
  liftPrim (modifySTRef' idRef (+ 1))
  tell (printf "NewRef#%d _" id_)
  pure (ConstRef v id_)
cleanExec (ReadRef ref) = do
  tell (printf "ReadRef %s" (formatRef ref))
  pure (crElem ref)
cleanExec (WriteRef ref _) = do
  tell (printf "WriteRef %s" (formatRef ref))
  pure ()
cleanExec (NewArray n v) = do
  idRef <- ask
  id_ <- liftPrim (readSTRef idRef)
  liftPrim (modifySTRef' idRef (+ 1))
  tell (printf "NewArray#%d %d _" id_ n)
  pure (ConstArr n v id_)
cleanExec (ReadArray arr i) = do
  tell (printf "ReadArray %s %d" (formatArr arr) i)
  pure (caElem arr)
cleanExec (WriteArray arr i _) = do
  tell (printf "WriteArray %s %d _" (formatArr arr) i)
  pure ()
cleanExec (GetArrayLength arr) = do
  tell (printf "GetArrayLength %s" (formatArr arr))
  pure (caSize arr)
cleanExec (Trace msg) = do
  tell (printf "Trace %s" msg)
  pure ()

formatRef :: ConstRef a -> String
formatRef ref = printf "ref#%d" (crID ref)

formatArr :: ConstArr a -> String
formatArr arr = printf "arr#%d" (caID arr)

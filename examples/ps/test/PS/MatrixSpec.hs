{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}

module PS.MatrixSpec (spec) where

import Control.Applicative ((<|>))
import Control.Monad (replicateM, when)
import Control.Monad.Except (ExceptT, lift, runExceptT)
import Data.Either (isLeft)
import Data.Function ((&))
import Data.List (transpose)
import FreeP (FreeP)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import qualified Test.Hspec as H
import Test.Hspec.Hedgehog (Gen, diff, forAll, hedgehog, (===))

import PS.IO (PSIOEff)
import PS.Matrix

import Util

spec :: H.Spec
spec = do
  H.describe "newMatrixFromLists" $ do
    H.context "creates a new matrix from a list of lists" $ do
      let list =
            [ [1, 7.8, 2, 3, 9]
            , [4, 7.1, 5, 6, 1.5]
            , [7, 7.9, 8, 9, 3.4]
            ]
      checkOnAllRunners $ \runInIO -> do
        eitherRes <- runInIO (runExceptT (getMatrixMetricsFromList list))
        eitherRes `H.shouldBe` Right (3, 5, list)

      let trList = transpose list
      checkOnAllRunners $ \runInIO -> do
        eitherRes <- runInIO (runExceptT (getMatrixMetricsFromList trList))
        eitherRes `H.shouldBe` Right (5, 3, trList)

    H.context "fails when the list is empty" $ do
      let list = []
      checkOnAllRunners $ \runInIO -> do
        eitherRes <- runInIO (runExceptT (getMatrixMetricsFromList list))
        eitherRes `H.shouldSatisfy` isLeft

    H.context "fails when rows are of different size" $ do
      let list =
            [ [8, 9, 4.5]
            , [1.2, 3]
            ]
      checkOnAllRunners $ \runInIO -> do
        eitherRes <- runInIO (runExceptT (getMatrixMetricsFromList list))
        eitherRes `H.shouldSatisfy` isLeft

    H.context "fails when all rows are empty (what even is that)" $ do
      let list = [[], [], [], []]
      checkOnAllRunners $ \runInIO -> do
        eitherRes <- runInIO (runExceptT (getMatrixMetricsFromList list))
        eitherRes `H.shouldSatisfy` isLeft

  H.describe "newSquareMatrixFromLists" $ do
    H.context "makes a square matrix out of a list of lists" $ do
      let list =
            [ [5, 8.9]
            , [3.7, 8]
            ]
      checkOnAllRunners $ \runInIO -> do
        eitherRes <- runInIO (runExceptT (getSquareMatrixMetricsFromList list))
        eitherRes `H.shouldBe` Right (2, list)

  H.describe "hstack" $ do
    H.context "puts two matrices side by side: hstack A B = [A B], " $ do
      H.context "works for some small matrices" $ do
        let lA =
              [ [5, 8.4, 3]
              , [-9, 3.2, 0.5]
              ]
            lB =
              [ [3, 6.9]
              , [15.02, -89]
              ]
            lC = zipWith (++) lA lB

        checkOnAllRunners $ \runInIO -> do
          lC' <- runInIO $ do
            mA <- newMatrixFromLists lA
            mB <- newMatrixFromLists lB
            hstack mA mB >>= to2dList
          lC' `H.shouldBe` lC

  H.describe "inverse" $ do
    H.context "calculates a square matrix inverse" $ do
      H.context "inverse of a 1x1 matrix: [[n]] is [[1/n]] " $ do
        checkOnAllRunners $ \runInIO -> hedgehog $ do
          f <- forAll genNonCloseToZeroDouble
          invL <- runInIO $ do
            m <- newSquareMatrixFromLists [[f]]
            invM <- inverse m
            to2dList invM
          invL === [[recip f]]

      H.context "inverse of a 2x2 matrix matches analytic solution" $ do
        let l = [[1, 2], [3, 4]]
        checkOnAllRunners $ \runInIO -> do
          invL <- runInIO $ do
            m <- newSquareMatrixFromLists l
            invM <- inverse m
            to2dList invM
          invL `H.shouldSatisfy` areCloseMatrices [[-2, 1], [1.5, -0.5]]

        checkOnAllRunners $ \runInIO -> hedgehog $ do
          [a, b, c, d] <- forAll (Gen.int (Range.linear (-100) 100))
                          & replicateM 4
                          & fmap (map fromIntegral)
          let det = a * d - b * c
          when (abs det > 0) $ do
            let expectedInv :: [[Elem]]
                expectedInv = [[d / det, -b / det] , [-c / det, a / det]]
            actualInv <- runInIO $ do
              m <- newSquareMatrixFromLists [[a, b], [c, d]]
              invM <- inverse m
              to2dList invM
            diff actualInv areCloseMatrices expectedInv

getMatrixMetricsFromList
  :: [[Elem]] -> ExceptT String (FreeP PSIOEff) (Int, Int, [[Elem]])
getMatrixMetricsFromList list = do
  m <- safeNewMatrixFromLists list
  list2d <- lift (to2dList m)
  pure (matrixHeight m, matrixWidth m, list2d)

getSquareMatrixMetricsFromList
  :: [[Elem]] -> ExceptT String (FreeP PSIOEff) (Int, [[Elem]])
getSquareMatrixMetricsFromList list = do
  m <- safeNewSquareMatrixFromLists list
  list2d <- lift (to2dList m)
  pure (matrixSide m, list2d)

genNonCloseToZeroDouble :: Gen Double
genNonCloseToZeroDouble =
  Gen.double (Range.linearFrac (-100) (-0.5))
  <|> Gen.double (Range.linearFrac 0.5 100)

eps :: Double
eps = 1e-8

areClose :: Double -> Double -> Bool
areClose a b = abs (a - b) <= eps

areCloseMatrices :: [[Elem]] -> [[Elem]] -> Bool
areCloseMatrices m1 m2 = and (zipWith (\r1 r2 -> and (zipWith areClose r1 r2)) m1 m2)

module PS.IOSpec (spec) where

import Data.Foldable (for_)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import qualified Test.Hspec as H
import Test.Hspec.Hedgehog (forAll, hedgehog, (===))

import PS
import PS.String (represent)

import Util (checkOnAllRunners)

spec :: H.Spec
spec = do
  H.describe "in PSIO" $ do
    H.context "actions are ordered properly" $ do
      let expr0 = do
            ref1 <- newRef (1 :: Int)
            ref2 <- newRef 2
            (do {v1 <- readRef ref1; writeRef ref2 (v1 + 5)}) *> writeRef ref2 100
            (,) <$> readRef ref1 <*> readRef ref2

      H.specify "representation of expr0 is correct" $ do
        lines (represent expr0) `H.shouldBe`
          [ "Step ( (NewRef#0 _) )"
          , "Step ( (NewRef#1 _) )"
          , "Step ( (ReadRef ref#0) )"
          , "Step ( (WriteRef ref#1) (WriteRef ref#1) )"
          , "Last ( (ReadRef ref#0) (ReadRef ref#1) )"
          ]

      H.context "execution result of expr0 is correct" $ do
        checkOnAllRunners $ \runInIO -> do
          (v1, v2) <- runInIO expr0
          (v1, v2) `H.shouldBe` (1, 100)

      let l = 10
      let expr1 = do
            arr <- newArrayFromList [1..l]
            for_ [0..l - 1] $ \i -> do
              oldV <- readArray arr i
              writeArray arr i (oldV * 2)
              for_ [i+1..l - 1] $ \j -> do
                prev <- readArray arr j
                writeArray arr j (prev + oldV)
            pure arr

      H.context "execution result of expr1 is correct" $ do
        checkOnAllRunners $ \runInIO -> do
          asList <- runInIO (expr1 >>= arrayToList)
          asList `H.shouldBe` [2^n - 2 | n <- [2..l + 1]]

    H.context "newArrayFromList xs >>= arrayToList ≡ xs" $ do
      checkOnAllRunners $ \runInIO -> hedgehog $ do
        xs <- forAll (Gen.list (Range.linear 0 100) genDouble)
        xs' <- runInIO (newArrayFromList xs >>= arrayToList)
        xs === xs'
 where
   genDouble = Gen.double (Range.linearFrac (-100) 100)

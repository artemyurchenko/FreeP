{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ApplicativeDo #-}

import Control.Monad (join, when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Writer (Writer, execWriter, runWriter, tell)
import Data.Array.IArray (bounds, (!))
import Data.Foldable (for_)
import Data.Function ((&))
import Data.Functor (($>))
import Data.IORef (newIORef, readIORef, writeIORef)
import Data.Vinyl.Core (Rec(..))
import Data.Vinyl.Functor (Identity(..))

import FreeP (FreeP, liftFreeP)
import GraphT (EffArray, GraphT, connect, inspectEff, inspectNodes)
import MultiExec (MultiExec, sequentialEff)
import Runners (Parallelizable(..), runParallelizable, runSideEffectFree, runWithDependenceGraph)

import Lib (Operation(..), checkPassword, printOp)

-- type FreeAp = Ap


-- type Free f = Free' (FreeAp f)

-- data Free' f a where
--   Last :: f a -> Free' f a
--   Step :: f x -> (x -> Free' f a) -> Free' f a

-- instance Applicative f =>
--          Applicative (Free' f) where
--   pure x = Last (pure x)
--   Last ff <*> Last fa = Last (ff <*> fa)
--   Last ff <*> Step fx next =
--     Step ((,) <$> ff <*> fx)
--          (\(f, x) -> fmap f (next x))
--   Step fx next <*> rhs =
--     Step fx (\x -> next x <*> rhs)

type Lang = FreeP Operation
say :: String -> Lang ()
input :: Lang String
readTimestamp :: String -> Lang Integer
updateTimestamp :: String -> Lang ()

say s = liftFreeP (Say s)
input = liftFreeP Input
readTimestamp filepath =
  liftFreeP (ReadTimestamp filepath)
updateTimestamp filepath =
  liftFreeP (UpdateTimestamp filepath)

code1 :: Lang ()
code1 = do
  say "Enter username"
  user <- input
  say ("Enter password for " <> user)
  pwd <- input
  when (checkPassword user pwd) $ do
    say ("Welcome, " <> user)

code2 :: Lang ()
code2 = do
  say "Enter username"
  user <- input
  say ("Enter password for " <> user)
  pwd <- input
  when (checkPassword user pwd) $ do
    say ("Welcome, " <> user)
    timestamp <- readTimestamp ("last." <> user)
    updateTimestamp ("last." <> user)
    say ("Last logged in at " <> show timestamp)

instance Parallelizable Operation where
  independent Say{} Say{} = False
  independent Input Input = False
  independent Input Say{} = False
  independent Say{} Input = False
  independent (ReadTimestamp file) (UpdateTimestamp file')
    = file /= file'
  independent (UpdateTimestamp file) (ReadTimestamp file')
    = file /= file'
  independent (UpdateTimestamp file) (UpdateTimestamp file')
    = file /= file'
  independent _ _ = True

execOperation :: Operation a -> Writer String a
execOperation op = let (pres, res) = printOp op in tell pres $> res

multiExecOperation' :: MultiExec Operation (Writer String) rs
multiExecOperation' (RNil) = pure RNil
multiExecOperation' (a :& RNil) = do
  av <- execOperation a
  pure (Identity av :& RNil)
multiExecOperation' (a :& b :& rest) = do
  restv <- multiExecOperation' (b :& rest)
  tell ", "
  av <- execOperation a
  pure (Identity av :& restv)

multiExecOperation :: MultiExec Operation (Writer String) rs
multiExecOperation op = multiExecOperation' op <* tell "\n"

buildGraph :: GraphT Operation IO ()
buildGraph = join (inspectNodes inspector)
  where
    inspector nodes = do
      let (st, fin) = bounds nodes
      lastIOIndexRef <- liftIO (newIORef (-1))
      for_ [st..fin] $ \i -> do
        let
          handleIOOp :: GraphT Operation IO ()
          handleIOOp = do
            lastIOIndex <- liftIO (readIORef lastIOIndexRef)
            when (lastIOIndex /= -1) $ do
              connect i lastIOIndex
            liftIO (writeIORef lastIOIndexRef i)

          handleOp :: Operation a -> GraphT Operation IO ()
          handleOp Say{} = handleIOOp
          handleOp Input = handleIOOp
          handleOp _ = pure ()

        inspectEff handleOp (nodes ! i)

multiExecOperationEff' :: EffArray Operation -> Writer String (EffArray Identity)
multiExecOperationEff' arr = sequentialEff execOperation' arr <* tell "\n"
  where
    execOperation' el = execOperation el <* tell ", "

multiExecOperationEff :: EffArray Operation -> IO (EffArray Identity)
multiExecOperationEff arr =
  multiExecOperationEff' arr
  & runWriter
  & (\(res, out) -> putStr out $> res)

printExecutionOrderNoOracle :: FreeP Operation a -> String
printExecutionOrderNoOracle op
  = op
  & runSideEffectFree multiExecOperation
  & execWriter

printExecutionOrderPar :: FreeP Operation a -> String
printExecutionOrderPar op
  = op
  & runParallelizable multiExecOperation
  & execWriter

printExecutionOrderGraph :: FreeP Operation a -> IO a
printExecutionOrderGraph =
  runWithDependenceGraph buildGraph multiExecOperationEff

main :: IO ()
main = do
  putStrLn "The new free monad"
  putStrLn "-----\ncode1, no oracle order:"
  putStrLn (printExecutionOrderNoOracle code1)
  putStrLn "-----\ncode2, no oracle order:"
  putStrLn (printExecutionOrderNoOracle code2)
  putStrLn "-----\ncode2, parallelizable order:"
  putStrLn (printExecutionOrderPar code2)
  putStrLn "-----\ncode2, dependence graph order:"
  printExecutionOrderGraph code2

{-# LANGUAGE RankNTypes #-}

module Main (main) where

import Control.Concurrent.Async.Pool (withTaskGroup)
import Criterion.Main (bench, bgroup, defaultMain, nfIO)
import Data.Vinyl (Rec)
import Data.Vinyl.Functor (Identity)

import qualified MultiExec
import Runners (runParallelizable, runSideEffectFree)

import Blog (Html(..), blog)
import MockData (requestVal)
import Types (Request)

mockExec :: Applicative f => Request a -> f a
mockExec = pure . requestVal

runMock
  :: Monad m
  => ((forall b. Request b -> m b) -> forall rs. Rec Request rs -> m (Rec Identity rs))
  -> m Html
runMock mkMultiExec = runParallelizable (mkMultiExec mockExec) blog

runMock'
  :: Monad m
  => ((forall b. Request b -> m b) -> forall rs. Rec Request rs -> m (Rec Identity rs))
  -> m Html
runMock' mkMultiExec = runSideEffectFree (mkMultiExec mockExec) blog

runWithNSlots :: Int -> IO Html
runWithNSlots n = withTaskGroup n (\tg -> runMock (MultiExec.inTaskGroup tg))

runWithNSlots' :: Int -> IO Html
runWithNSlots' n = withTaskGroup n (\tg -> runMock' (MultiExec.inTaskGroup tg))

main :: IO ()
main = defaultMain
  [ bgroup "runParallelizable"
    [ bgroup "mock data"
      [ bench "sequential" (nfIO (runMock MultiExec.sequential))
      , bench "allConcurrent" (nfIO (runMock MultiExec.allConcurrent))
      , bench "inTaskGroup 1" (nfIO (runWithNSlots 1))
      , bench "inTaskGroup 2" (nfIO (runWithNSlots 2))
      , bench "inTaskGroup 4" (nfIO (runWithNSlots 4))
      ]
    ]
  , bgroup "runSideEffectFree"
      [ bench "sequential" (nfIO (runMock' MultiExec.sequential))
      , bench "allConcurrent" (nfIO (runMock' MultiExec.allConcurrent))
      , bench "inTaskGroup 1" (nfIO (runWithNSlots' 1))
      , bench "inTaskGroup 2" (nfIO (runWithNSlots' 2))
      , bench "inTaskGroup 4" (nfIO (runWithNSlots' 4))
      ]
  ]

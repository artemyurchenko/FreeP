{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RankNTypes #-}

module MultiExec
  ( sequential
  , allConcurrent
  , inTaskGroup
  , sequentialEff
  , allConcurrentEff
  , inTaskGroupEff
  , MultiExec
  ) where

import qualified Control.Concurrent.Async as NoPool (Concurrently(..))
import Control.Concurrent.Async.Pool (TaskGroup)
import qualified Control.Concurrent.Async.Pool as Pool (mapConcurrently, wait, withAsync)
import Control.Monad.IO.Unlift (MonadUnliftIO(..))
import Data.Coerce (coerce)
import Data.Vinyl.Core (Rec(..), rtraverse)
import Data.Vinyl.Functor (Identity(..))

import GraphT (EffArray, traverseEff)

type MultiExec f m rs = Rec f rs -> m (Rec Identity rs)

sequential
  :: Applicative m => (forall b. f b -> m b) -> MultiExec f m rs
sequential exec = rtraverse (fmap Identity . exec)

allConcurrent
  :: MonadUnliftIO m
  => (forall b. f b -> m b) -> MultiExec f m rs
allConcurrent exec actions = withRunInIO $ \runInIO -> NoPool.runConcurrently
  (rtraverse (NoPool.Concurrently . coerce . runInIO . exec) actions)

-- Unfortunately, the versions below don't work:
-- https://github.com/jwiegley/async-pool/issues/23
{-

inTaskGroup
  :: MonadUnliftIO m
  => TaskGroup -> (forall b. f b -> m b) -> Rec f rs -> m (Rec Identity rs)
inTaskGroup tg exec actions = withRunInIO $ \runInIO -> Pool.runConcurrently
  (rtraverse (Pool.Concurrently . const . coerce . runInIO . exec) actions) tg

inTaskGroupEff
  :: MonadUnliftIO m
  => TaskGroup -> (forall b. f b -> m b) -> EffArray f -> m (EffArray Identity)
inTaskGroupEff tg exec actions = withRunInIO $ \runInIO -> Pool.runConcurrently
  (traverse (traverseEff (Pool.Concurrently . const . runInIO . exec)) actions) tg
-}

inTaskGroup
  :: forall f m rs. MonadUnliftIO m
  => TaskGroup -> (forall b. f b -> m b) -> MultiExec f m rs
inTaskGroup tg exec = rtraverseAsync tg (fmap Identity . exec)

rtraverseAsync
  :: forall f h g rs. (MonadUnliftIO h)
  => TaskGroup -> (forall x. f x -> h (g x)) -> Rec f rs -> h (Rec g rs)
rtraverseAsync tg f rs = withRunInIO (`go` rs)
  where
    go :: (forall a. h a -> IO a) -> Rec f rs' -> IO (Rec g rs')
    go _ RNil = pure RNil
    go runInIO (x :& xs) = Pool.withAsync tg (runInIO (f x)) $ \ax -> do
      next <- runInIO (rtraverseAsync tg f xs)
      executedX <- Pool.wait ax
      pure (executedX :& next)

sequentialEff
  :: forall f m
  . Applicative m
  => (forall b. f b -> m b) -> EffArray f -> m (EffArray Identity)
sequentialEff exec = traverse (traverseEff exec)

allConcurrentEff
  :: MonadUnliftIO m
  => (forall b. f b -> m b) -> EffArray f -> m (EffArray Identity)
allConcurrentEff exec actions = withRunInIO $ \runInIO -> NoPool.runConcurrently
  (traverse (traverseEff (NoPool.Concurrently . runInIO . exec)) actions)

inTaskGroupEff
  :: MonadUnliftIO m
  => TaskGroup -> (forall b. f b -> m b) -> EffArray f -> m (EffArray Identity)
inTaskGroupEff tg exec effs = withRunInIO
  (\runInIO -> Pool.mapConcurrently tg (traverseEff (runInIO . exec)) effs)

{- TODO make a dirty executor. it has executor that can evaluate
 separate operations and put results into it (stateful) and a finisher
 that walks unevaluated operations and "finishes" (evaluates) them on
 a case per case basis. the executor is a free monad that has
 operations like, observe, putSucess and stuff. Might want to use
 MonadUnliftIO.

dirtyExec finisher executor

-}

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Runners
  ( runSideEffectFree
  , runParallelizable
  , runWithDependenceGraph
  , Parallelizable(..)
  , runSideEffectFree'
  ) where

import Data.Vinyl.Core (Rec(..))
import Data.Vinyl.Curry (Curried, runcurry')
import Data.Vinyl.Functor (Identity(..))

import FreeP (FreeAp(..), FreeP, foldFree)
import GraphT (runWithDependenceGraph)

runSideEffectFree
  :: forall f m a. Monad m
  => (forall rs. Rec f rs -> m (Rec Identity rs))
  -> FreeP f a -> m a
runSideEffectFree multiExec = foldFree (go RNil)
  where
    go :: Rec f rs -> FreeAp f (Curried rs a') -> m a'
    go inds (ImpureA fx next) = go (fx :& inds) next
    go inds (PureA x) = runcurry' x <$> multiExec inds

runSideEffectFree'
  :: forall f m a. Monad m
  => (forall x. FreeAp f x -> m x)
  -> FreeP f a -> m a
runSideEffectFree' = foldFree

-- | The only meaningful property is symmetricity
class Parallelizable f where
  independent :: f a -> f b -> Bool

runParallelizable
  :: forall f m a. (Parallelizable f, Monad m)
  => (forall rs. Rec f rs -> m (Rec Identity rs))
  -> FreeP f a -> m a
runParallelizable multiExec = foldFree (runParallelizableAp multiExec)

runParallelizableAp
  :: forall f m a. (Parallelizable f, Applicative m)
  => (forall rs. Rec f rs -> m (Rec Identity rs)) -> FreeAp f a -> m a
runParallelizableAp multiExec = go RNil
  where
    go :: Rec f rs -> FreeAp f (Curried rs a') -> m a'
    go inds (ImpureA fx next)
      | rall (independent fx) inds = go (fx :& inds) next
    go inds next = flip runcurry' <$> multiExec inds <*> goOn next

    goOn :: FreeAp f a' -> m a'
    goOn (PureA x) = pure x
    goOn next = go RNil next

rall :: (forall x. f x -> Bool) -> Rec f rs -> Bool
rall _ RNil = True
rall cond (fx :& rest) = cond fx && rall cond rest

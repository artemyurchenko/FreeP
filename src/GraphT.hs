{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ScopedTypeVariables #-}

module GraphT
  ( GraphT
  , Eff
  , EffArray
  , inspectNodes
  , inspectEff
  , mapEff
  , liftEff
  , traverseEff
  , connect
  , runWithDependenceGraph
  ) where

import Control.Monad (filterM)
import Control.Monad.Reader (MonadReader, MonadTrans, ReaderT, ask, lift, runReaderT)
import Control.Monad.ST (ST, runST)
import Control.Monad.State (MonadState, StateT, execStateT, modify')
import Control.Monad.Trans.Free.Church (FT, iterTM, liftF)
import Data.Array (Array)
import Data.Array.IArray (IArray, array, bounds, listArray)
import qualified Data.Array.IArray as Array (elems, (!))
import Data.Array.ST (STUArray, getAssocs, newArray, readArray, writeArray)
import Data.Foldable (for_)
import Data.Function ((&))
import Data.IntMap (IntMap)
import qualified Data.IntMap as Map (alter, empty)
import Data.IntSet (IntSet)
import qualified Data.IntSet as Set (foldl', fromDistinctAscList, fromList, null, toList)
import Data.Vinyl.Core (RMap(..), Rec(..), RecordToList(..))
import Data.Vinyl.Curry (Curried, runcurry')
import Data.Vinyl.Functor (Const(..), Identity(..))
import Lens.Micro.Platform (_Just, at, (^.))
import Unsafe.Coerce (unsafeCoerce)

import FreeP (FreeAp(..), FreeP, foldFree)

runWithDependenceGraph
  :: forall f m a. Monad m
  => GraphT f m ()
  -> (EffArray f -> m (EffArray Identity))
  -> FreeP f a
  -> m a
runWithDependenceGraph buildGraph multiExec = foldFree (go RNil)
  where
    go :: (RecordToList rs, RMap rs) => Rec f rs -> FreeAp f (Curried rs a') -> m a'
    go effRec (ImpureA fx next) = go (fx :& effRec) next
    go effRec (PureA f) = do
      okayList <- executeEffRec effRec
      pure (unsafeApplyEffs effRec f okayList)

    executeEffRec :: (RecordToList rs, RMap rs) => Rec f rs -> m [Eff Identity]
    executeEffRec effRec = do
      let effArray = effsToEffArray effRec
      let work = interpretGraph buildGraph
      adjMap <- runGraphTImpl work effArray
      let pools = makePools (bounds effArray) adjMap
      results <- executePools pools effArray multiExec
      pure (Array.elems results)

-- a better name would be SomeF but here we are
data Eff f where
  Eff :: f a -> Eff f

inspectEff :: (forall a. f a -> r) -> Eff f -> r
inspectEff f (Eff fx) = f fx

mapEff :: (forall a. f a -> g a) -> Eff f -> Eff g
mapEff f (Eff fx) = Eff (f fx)

liftEff :: Functor f => Eff f -> f (Eff Identity)
liftEff (Eff fa) = fmap (Eff . Identity) fa

traverseEff :: Functor g => (forall a. f a -> g a) -> Eff f -> g (Eff Identity)
traverseEff f = liftEff . mapEff f

type EffArray f = Array Int (Eff f)

effsToEffArray :: (RMap rs, RecordToList rs) => Rec f rs -> EffArray f
effsToEffArray effs
  = effs
  & rmap (Const . Eff)
  & recordToList
  & listToArray

data GraphEff f a where
  InspectNodes
    :: (forall arr. IArray arr (Eff f) => arr Int (Eff f) -> a) -> GraphEff f a
  Connect :: Int -> Int -> a -> GraphEff f a

deriving instance Functor (GraphEff f)

inspectNodes :: (forall arr. IArray arr (Eff f) => arr Int (Eff f) -> a) -> GraphT f m a
inspectNodes cont = liftF (InspectNodes cont)

connect :: Int -> Int -> GraphT f m ()
connect i j = liftF (Connect i j ())

type AdjMap = IntMap [Int]

type GraphT f = FT (GraphEff f)

newtype GraphTImpl f m a = GraphTImpl (StateT AdjMap (ReaderT (EffArray f) m) a)
  deriving newtype ( Functor
                   , Applicative
                   , Monad
                   , MonadState AdjMap
                   , MonadReader (EffArray f)
                   )

instance MonadTrans (GraphTImpl f) where
  lift = GraphTImpl . lift . lift

interpretGraph
  :: forall f m a. Monad m => FT (GraphEff f) m a -> GraphTImpl f m a
interpretGraph = iterTM exec
  where
    exec :: GraphEff f (GraphTImpl f m a) -> GraphTImpl f m a
    exec (InspectNodes cont) = ask >>= cont
    exec (Connect i j cont) = modify' addArc >> cont
      where
        -- dependency arcs always go from the lesser to the greater,
        -- because that's how it is in sequential execution, semantics
        -- of which we are trying to match
        (parent, child) = (min i j, max i j)
        addArc = Map.alter addChild parent
        addChild whatever | i == j = whatever -- we don't want a loop
        addChild Nothing = Just [child]
        addChild (Just children) = Just (child : children)

runGraphTImpl :: Monad m => GraphTImpl f m a -> EffArray f -> m AdjMap
runGraphTImpl (GraphTImpl work) effArray
  = work
  & flip execStateT Map.empty
  & flip runReaderT effArray

makePools :: (Int, Int) -> AdjMap -> [IntSet]
makePools (st, fin) adjacency = runST $ do
  depCounts <- countDependencies
  initialPool <- getInitialPool depCounts
  computePools depCounts initialPool
  where
    countDependencies :: ST s (STUArray s Int Int)
    countDependencies = do
      deps <- newArray (st, fin) 0
      for_ adjacency $ \children ->
        for_ children (plusOne deps)
      pure deps

    getInitialPool :: STUArray s Int Int -> ST s IntSet
    getInitialPool depCounts = do
      assocs <- getAssocs depCounts
      assocs
        & filter (\(_, depCount) -> depCount == 0)
        & map fst
        & Set.fromDistinctAscList
        & pure

    computePools :: STUArray s Int Int -> IntSet -> ST s [IntSet]
    computePools depCounts = go []
      where
        go acc pool | Set.null pool = pure acc
        go acc pool = do
          updateDeps
          pool' <- getNewPool
          go (pool : acc) pool'
          where
            updateDeps =
              intSetFor_ pool $ \parent ->
                for_ (adjacency ^. at parent . _Just) (minusOne depCounts)

            getNewPool =
              pool
                & Set.toList
                & foldMap (\i -> adjacency ^. at i . _Just)
                & filterM hasZeroDeps
                & fmap Set.fromList

            hasZeroDeps node = do
              deps <- readArray depCounts node
              pure (deps == 0)

    plusOne arr i = do
      v <- readArray arr i
      writeArray arr i (v + 1)

    minusOne arr i = do
      v <- readArray arr i
      writeArray arr i (v - 1)

executePools
  :: forall f m
  . Monad m
  => [IntSet]
  -> EffArray f
  -> (EffArray f -> m (EffArray Identity))
  -> m (EffArray Identity)
executePools pools effArray multiExec
  = pools
  & traverse executePoolWithAssociations
  & fmap concat
  & fmap (array (st, fin)) -- UNSAFE in theory, prove that safe here
  where
    executePoolWithAssociations isSet = do
      let is = Set.toList isSet
      mpool <- multiExec (areaGet effArray is)
      pure (zip is (Array.elems mpool))

    (st, fin) = bounds effArray

unsafeApplyEffs
  :: forall proxy rs a
  . Rec proxy rs -> Curried rs a -> [Eff Identity] -> a
unsafeApplyEffs proxyRec f effs = runcurry' f (buildOkayRec proxyRec effs)
  where
    -- FIXME not efficient, it's better to accumulate, but requires
    -- more type level magic
    buildOkayRec :: Rec proxy rs' -> [Eff Identity] -> Rec Identity rs'
    buildOkayRec RNil [] = RNil
    buildOkayRec (_ :& proxyRest) (eff : rest)
      = inspectEff unsafeCoerce eff :& buildOkayRec proxyRest rest
    buildOkayRec _ _ = error "your proxyRec and effs are of differen sizes =("

areaGet :: Array Int e -> [Int] -> Array Int e
areaGet arr is
  = is
  & map (arr Array.!)
  & listToArray

listToArray :: IArray a e => [e] -> a Int e
listToArray l = listArray (1, length l) l

intSetFor_ :: Applicative f => IntSet -> (Int -> f b) -> f ()
intSetFor_ set f = Set.foldl' order (pure ()) set
  where
    order acc el = acc <* f el
